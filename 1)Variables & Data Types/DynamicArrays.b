     PROGRAM DYNAMICARRAYS

     website = "klavyedeparmaklar.com"
     address = "London, UK"

     employee = "Sebahattin Kalach":@FM:address:@FM:27:@VM:website:@VM:13.31
     CRT @(-1)
     CRT employee
     PRINT employee
     PRINT FMT(employee, 'MCP')
     CRT "Address is ": employee<2> ;* The address is in the second field

     CRT @(-1)
     LOAN_REQUEST = '001-Jack-GBP-20210101-240-0.01'
     CONVERT '-' TO @FM IN LOAN_REQUEST
     CRT FMT(LOAN_REQUEST, 'MCP')
     CRT 'CUSTOMER: ': LOAN_REQUEST<2>
     CRT 'CURRENCY: ': LOAN_REQUEST<3>

  END