     PROGRAM StringTest

     name = "Jack Smith"
     company = 'jsmith.com'
     address = \SE1 1LN, Redcross Way \

     CRT name
     CRT company
     CRT address

     CRT "My name is ": name : " I work for ": company
     CRT "My address is ": address

     post_code = address[1,7]
     street = address[10,22]
     CRT "Post Code: ": post_code
     CRT "Street: ": street

     CRT "IN INFO BASIC All THE VARIABLES ARE MUTABLE!!"
     name[5] = "TEST"
     CRT name
     CRT @(-1) ;*clear screen

     upperCase = "lower"
     CRT upperCASE
     CRT "UPCASE Function: ":UPCASE(upperCase)
     CRT "LEN Function: ":LEN(upperCase)
     CRT "STR Function: ":STR("A", 2)

  END