     SUBROUTINE TB.PRACTICE.SUBROUTINE(CUST_ID, CUST_NAME, CUST_NATIONALITY, CUST_POSTCODE)

*---------------------------------------------------------
*- SUBROUTINE to fetch some customer details             
*- @in  : CUST_ID
*- @out : CUST_NAME, CUST_NATIONALITY, CUST_POSTCODE
*- Author: Sebahattin KALACH
*- Date: 20210409
*---------------------------------------------------------

     $INSERT T24.BP I_COMMON
     $INSERT T24.BP I_EQUATE
     $INSERT T24.BP I_F.DATES
     $INSERT T24.BP I_F.CUSTOMER

     FN_CUST = 'F.CUSTOMER'
     F_CUST = ''
     CUST_REC = ''

     CUST_NAME = ''
     CUST_NATIONALITY = ''
     CUST_POSTCODE = ''

     CALL LOAD.COMPANY('BNK')
     CALL OPF(FN_CUST, F_CUST)
     CALL F.READ(FN_CUST, CUST_ID, CUST_REC, F_CUST, ЕRR)

     CUST_NAME = CUST_REC<EB.CUS.SHORT.NAME>
     CUST_NATIONALITY = CUST_REC<EB.CUS.NATIONALITY>
     CUST_POSTCODE = CUST_REC<EB.CUS.POST.CODE>

     RETURN
  END